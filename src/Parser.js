import HTMLParser from "./Parser/HTMLParser.js";
import XMLParser from "./Parser/XMLParser.js";
import TurnToMarkdown from "./Parser/TurnToMarkdown.js";

export default { HTMLParser, XMLParser, TurnToMarkdown };
