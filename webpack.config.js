module.exports = {
    entry: "./JSpider-webpack.js",
    output: {
        filename: "JSpider.webpack.js",
        path: __dirname + "/dist",
    },
    mode: "production",
};
