import searchWindow from "./searchWindow.js";
import AjaxHook from "./AjaxHook.js";

export default { searchWindow, AjaxHook };
